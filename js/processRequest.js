//Core Modules
//var http = require('http');
var fs   = require('fs');
var url  = require('url');

//User Modules
//var pageCreator = require('./pageCreator.js');

//api used by server.js
var api = {
    requestedFile: function(request, response){
      if (request.method == 'GET'){
        requestedFile_get(request, response);
      }
      else {
        console.log('[' + request.method + '] method not handled'); 
        errPage(response);
      }
  }
};


//function passed to 'pageCreator' functions
//returns created script to the user
// function respondWithScript(err, request, response, script){
//   if (err) {
//     console.log('err creating ', request.url);
//     errPage(response);
//     return;
//   }
//   response.writeHead(200, {'Content-Type': 'application/javascript'});
//   response.write(script, 'utf-8');
//   response.end();
// }



//GET used when user requires new page or resource
function requestedFile_get(request, response){

  var clientID = getClientID(request);

  //main.css file request
  if (cleanUrl(request.url) == '/main.css'){
    fs.readFile('../css/main.css', function (err, cssPage) {
      if (err) {
        console.log('main.css file not found');
        errPage(response);
      }
      else {
        response.writeHead(200, {'Content-Type': 'text/css' });
        response.write(cssPage, 'utf-8');
        response.end();
      }
    });
  }

  //new user Page request
  else if (cleanUrl(request.url) == '/newuser.html'){
    fs.readFile('../html/newuser.html', function (err, webPage) {
      if (err) {
        console.log('newuser.html file not found');
        errPage(response);
      }
      else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(webPage, 'utf-8');
        response.end();
      }
    });
  }

  //change name Page request
  else if (cleanUrl(request.url) == '/changename.html'){
    fs.readFile('../html/changename.html', function (err, webPage) {
      if (err) {
        console.log('changename.html file not found');
        errPage(response);
      }
      else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(webPage, 'utf-8');
        response.end();
      }
    });
  }

  //reconnect Page request
  else if (cleanUrl(request.url) == '/reconnect.html'){
    fs.readFile('../html/reconnect.html', function (err, webPage) {
      if (err) {
        console.log('reconnect.html file not found');
        errPage(response);
      }
      else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(webPage, 'utf-8');
        response.end();
      }
    });
  }

  //chat Page request
  else if (isChatRequest(request)){
    fs.readFile('../html/chat.html', function (err, webPage) {
      if (err) {
        console.log('chat.html file not found');
        errPage(response);
      }
      else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(webPage, 'utf-8');
        response.end();
      }
    });
  }

  //favion.ico request
  else if (cleanUrl(request.url) == '/favicon.ico'){    
    fs.readFile('./favicon.ico', function (err, img) {
      if (err) {
        console.log('./favicon.ico file not found');
        errPage(response);
      }
      else {
        response.writeHead(200, {'Content-Type': 'image/x-icon' });
        response.write(img, 'binary');
        response.end();
      }

    });
  }
  
  else if (clientID !== undefined) {
    console.log('valid user');
    //Home-Page request
    if (cleanUrl(request.url) == '/' || cleanUrl(request.url) == '/index.html'){
      //console.log(getUserID(request));
      fs.readFile('../html/index.html', function (err, webPage) {
        if (err) {
          console.log('index.html file not found');
          errPage(response);
        }
        else {
          response.writeHead(200, {'Content-Type': 'text/html'});
          response.write(webPage, 'utf-8');
          response.end();
        }
      });
    }
    else {
      errPage(response);
    }
  }

  //login-Page request
  else if (cleanUrl(request.url) == '/' || cleanUrl(request.url) == '/index.html'){
    //console.log(getUserID(request));
    fs.readFile('../html/login.html', function (err, webPage) {
      if (err) {
        console.log('login.html file not found');
        errPage(response);
      }
      else {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(webPage, 'utf-8');
        response.end();
      }
    });


  }








  else {
    errPage(response);
  }
}

function errPage(response){
  response.writeHead(404, {'Content-Type': 'text/plain'});
  response.end('error, bad url request');
}

function cleanUrl(givenUrl){
  return url.parse(givenUrl).pathname;
}

// function setCookie(cname,cvalue,exdays) {
//   var d = new Date();
//   d.setTime(d.getTime() + (exdays*24*60*60*1000));
//   var expires = "expires=" + d.toGMTString();
//   document.cookie = cname+"="+cvalue+"; "+expires;
// }

// function getCookie(cname) {
//   console.log('getting > ',cname);
//   var name = cname + "=";
//   var ca = document.cookie.split(';');
//   console.log(ca,'length=',ca.length);
//   for(var i=0; i<ca.length; i++) {
//     var c = ca[i];
//     while (c.charAt(0)==' ') c = c.substring(1);//remove leading spaces
//     if (c.indexOf(name) != -1) {
//       return c.substring(name.length, c.length);
//     }
//   }
//   return "";
// }

function isChatRequest(request){
  var path = cleanUrl(request.url).substr(0, 6);
  //console.log('path = ', path);
  if (path == '/chat-'){
    return true;
  }
  return false;
}

function getClientID(request){
  if (request.headers.cookie == undefined){
    return undefined;
  }
  cookieKey = "clientID=";
  var ca = request.headers.cookie.split(';');
  //console.log(ca,'length=',ca.length);
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') {
      c = c.substring(1);//remove leading spaces
    }
    if (c.indexOf(cookieKey) != -1) {
      return c.substring(cookieKey.length, c.length);
    }
  }
  return undefined;
}


module.exports = api;
