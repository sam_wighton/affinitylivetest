//Core Modules
var http = require('http');
var fs   = require('fs');
var url  = require('url');

var mongo  = require('mongodb').MongoClient;
var sockets = require('socket.io').listen(8000).sockets;

//User Modules
var processRequest = require('./processRequest.js');

var d = new Date();
var uniqueIdSeed = d.getTime();

var connectedClients = [];
var connectedClientsDetails = [];

//connect to database
mongo.connect('mongodb://127.0.0.1/chat', function(err, db){
  if(err) throw err;
  //**************************************************************************************************************
  //start chat server
  //**************************************************************************************************************
  sockets.on('connection', function(socket){
    //when client connects set them as 'online'
    connectedClients.push(socket);
    connectedClientsDetails.push({});
    //when user disconnect set them as 'offline'
    socket.on('disconnect', function() {
      var i = connectedClients.indexOf(socket);
      connectedClients.splice(i, 1);
      connectedClientsDetails.splice(i, 1);
    });
    //console.log(connectedClientsDetails);
    //var socketIDs = Object.keys(sockets.server.eio.clients);
    //console.log("sockets:", socketIDs );
    //console.log("sockets2:", sockets.connected[socketIDs[0]].client.ondata);
    //console.log("number of connected clients:", Object.keys(connectedClients).length);

    var col_messages = db.collection('messages');
    var col_clients  = db.collection('clients');
    var col_groups   = db.collection('groups');
    var sendStatus = function(s){
      socket.emit('status', s);
    };
    var replaceIdWithName = function(messageArray, callBackFunction){
      col_clients.find().toArray(function(err,clients){
        if (err) throw err;
        for (var i = 0; i < messageArray.length; i++){
          messageArray[i] = getName(clients,messageArray[i]);
        }
        //console.log("#1", messageArray);
        callBackFunction(messageArray);
      });
    }

    //wait for request for messages
    socket.on('requestMessages', function(data){
      //console.log("client requesting messages");
      col_clients.find({"clientID":data['clientID']}).toArray(function(err,clientArray){
        if (clientArray.length){
          var client = clientArray[0];
          var groupLinkArray = clientArray[0]['groupLinks'];
          var groupID = undefined;
          var groupLink = extractGroupLink(data['groupLinkID']); //get groupLink from url
          //console.log("group link = ", groupLink);
          //find what group the client wants messages for 
          for (var i = 0; i < groupLinkArray.length; i++){
            //console.log(groupLinkArray[i],"!=",groupLink);
            if (groupLinkArray[i]['groupLinkID'] == groupLink){
              groupID = groupLinkArray[i]['groupID'];
              break;
            }
          }
          //store what chat client is now in
          var i = connectedClients.indexOf(socket);
          connectedClientsDetails[i] = {"groupID":groupID,"clientID":client['clientID']};
          //console.log(connectedClientsDetails);

          //console.log('user', client.clientID, 'requested chat', groupID, 'with link', groupLink);
          //emit all messages
          if (groupID !== undefined){
            col_messages.find({"groupID":groupID}).sort({_id:1}).toArray(function(err,res){
              if (err) throw err;
              replaceIdWithName(res,function(messageArray){
                socket.emit('output', messageArray);
              });
            });
          }
        }
      });
    });



    //wait for input
    socket.on('input', function(data){
      //todo use ES6 functionality in the following lines
      var clientID  = data.clientID;
      var message = data.message;
      var whitespacePattern = /^\s*$/;

      //todo send less generic status
      if (whitespacePattern.test(message)){
        sendStatus("Message can't be only whitespace.");
      }
      else {
        var groupLink = extractGroupLink(data['groupLinkID']);
        col_clients.find({"clientID":clientID}).toArray(function(err,clientArray){
          var client  = clientArray[0];
          var groupID = getGroupFromGroupLink(client, groupLink);
          col_messages.insert({"clientID": clientID, "message": message, "groupID":groupID}, function(){
            //emit latest message to all clients
            replaceIdWithName([data],function(messageArray){
              //filter messages to go only to correct clients
              for (var i = 0; i < connectedClients.length; i++){
                //console.log(connectedClientsDetails[i]['groupID'], "!=", groupID);
                if (connectedClientsDetails[i]['groupID'] == groupID){
                  connectedClients[i].emit('output', messageArray);
                }
              }
              sendStatus({
                message: "Message sent",
                clear: true
              });
            });
          });
        });
      }
    });

    //wait for a request for a new clientID
    socket.on('requestClientID', function(){
      addClientAndGroups(socket, col_clients, col_groups);
    });

    //todo live updates as contacts come online
    //wait for a request for contacts
    socket.on('requestContacts', function(data){
      //console.log('before clients',data);
      var i = connectedClients.indexOf(socket);
      connectedClientsDetails[i] = {"clientID":data['clientID']};
      //fetch contact list for client
      col_clients.find().toArray(function(err,clientArray){
        var currentClientID = data['clientID'];
        col_groups.find().toArray(function(err,groupArray){
          var contactList = getGroupNames(currentClientID, clientArray, groupArray);
          socket.emit('responseContacts',contactList);
          //console.log(contactList);
          //********************************************************************************************************working here
          //todo filter contact list properly 

        });

      });
    });

    //wait for a request for chat name
    socket.on('requestChatName', function(data){
      //console.log('before clients',data);
      col_clients.find().toArray(function(err,clientArray){
        var currentClientID = data['clientID'];
        col_groups.find().toArray(function(err,groupArray){
          var contactList = getGroupNames(currentClientID, clientArray, groupArray);
          var currentGroupLinkID = extractGroupLink(data['groupLinkID']);
          //console.log('chatting with:', chattingWith(currentClientID, currentGroupLinkID, clientArray, groupArray));
          socket.emit('responseChatName', chattingWith(currentClientID, currentGroupLinkID, clientArray, groupArray));
        });
      });
    });


    //change users name in database
    socket.on('requestNewName', function(data){
      if (data['clientID'] !== undefined){
        col_clients.find({"clientID":data['clientID']}).toArray(function(err,clientArray){
          var client = clientArray[0];
          client['name'] = data['name'];
          col_clients.update({"clientID":client['clientID']},client,{});
        });
      }
    });
  });
  //**************************************************************************************************************
  //define http server
  httpServer = http.createServer(function (request, response) {
    console.log('[' + request.method + '] ' + request.url);
    //console.log(request.headers.cookie);
    processRequest.requestedFile(request, response);
  });

  //start http server
  httpServer.listen(8001);
});

function getUniqueID(){
  var x = Math.floor((uniqueIdSeed * uniqueIdSeed) / 1000 % 10000000000);
  uniqueIdSeed = x;
  return x.toString();
}


function getName(clients,message){
  for (var i = 0; i < clients.length; i++){
    if (message['clientID'] == clients[i]['clientID']){
      return {
        "message":message['message'],
        "name":clients[i]['name']
      };
    }
  }
  return {
    "message":message['message'],
    "name":"Name Not Found"
  };
}

function addClientAndGroups(socket, col_clients, col_groups){
  var newClientID = getUniqueID();
  //respond to new clients webpage
  socket.emit('responseClientID', newClientID);
  var newGroups = [];

  col_clients.find().toArray(function(err, clientArray){
    for (var i = 0; i < clientArray.length; i++){
      var newGroupID = getUniqueID();
      col_groups.insert({
        "groupID":newGroupID,
        "members":[newClientID,clientArray[i]['clientID']]
      });
      //add group link to list for new client
      newGroups.push({"groupLinkID": getUniqueID(), "groupID": newGroupID});

      //add group link to existing client
      var existingClientGL = clientArray[i]["groupLinks"];
      if (existingClientGL == undefined){
        existingClientGL = [{"groupLinkID": getUniqueID(), "groupID": newGroupID}];
      }
      else{
        existingClientGL.push({"groupLinkID": getUniqueID(), "groupID": newGroupID});
      }
      clientArray[i]["groupLinks"] = existingClientGL;
      //console.log(existingClientGL);
      col_clients.update({"clientID": clientArray[i]['clientID']},clientArray[i], function(){
        //console.log("existing client Updated");
      });
    }
    //add the new client to the database
    col_clients.insert({"clientID": newClientID, "name": getUniqueID(), "groupLinks":newGroups}, function(){
    });
  });
}

function extractGroupLink(givenString){
  //extract groupLink from given url
  return removeType(url.parse(givenString).pathname.substr(6));
}

function removeType(givenString){
  var index = givenString.indexOf('.');
  if (index != (-1)){
    return givenString.substr(0, index);
  }
  else return givenString;
}

//The following 4 functions determine the name of 
function getGroupNames(currentClientID, clientArray, groupArray){
  if (currentClientID == undefined){
    return "";
  }
  var currentClient = undefined;
  //get client
  for (var i = 0; i < clientArray.length; i++){
    //console.log(currentClientID,"!=",clientArray[i]['clientID']);
    if (currentClientID == clientArray[i]['clientID']){
      currentClient = clientArray[i];
      break;
    }
  }
  var groupNames = [];
  for (var i = 0; i < currentClient['groupLinks'].length; i++){
    var currentGroupName = getGroupName(currentClientID, currentClient['groupLinks'][i]['groupID'], groupArray,clientArray)
    groupNames.push({
      "name":currentGroupName,
      "groupLinkID":currentClient['groupLinks'][i]["groupLinkID"],
      "onlineStatus":isNameOnline(currentGroupName,clientArray,connectedClientsDetails)
    });
  }
  return groupNames;
}

function getGroupName(currentClientID, groupID, groupArray, clientArray){
  for (var i = 0; i < groupArray.length; i++){
    if (groupID == groupArray[i]["groupID"]){
      return getOtherMembers(currentClientID, groupArray[i]["members"], clientArray);
    }
  }
}

function getOtherMembers(currentClientID, members, clientArray){
  var returnString = "";
  for (var i = 0; i < members.length; i++){
    //console.log(currentClientID,"!=",members[i]);
    if (currentClientID != members[i]){
      returnString += getNameFromID(members[i], clientArray);
    }
  }
  return returnString;
}

function getNameFromID(clientID, clientArray){
  for (var i = 0; i < clientArray.length; i++){
    if (clientID == clientArray[i]["clientID"]){
      return clientArray[i]["name"];
    }
  }
  return "";
}

//return names of other members in the current chat
function chattingWith(currentClientID, currentGroupLinkID, clientArray, groupArray){
  if (currentClientID == undefined){
    return {"name":"Unknown"};
  }
  var currentClient = undefined;
  //get client
  for (var i = 0; i < clientArray.length; i++){
    //console.log(currentClientID,"!=",clientArray[i]['clientID']);
    if (currentClientID == clientArray[i]['clientID']){
      currentClient = clientArray[i];
      break;
    }
  }
  var currentGroup = undefined;
  for (var i = 0; i < currentClient['groupLinks'].length; i++){
    //console.log(currentGroupLinkID,"!=",currentClient['groupLinks'][i]['groupLinkID']);
    if (currentGroupLinkID == currentClient['groupLinks'][i]['groupLinkID']){
      return {
        "name": getGroupName(currentClientID, currentClient['groupLinks'][i]['groupID'], groupArray,clientArray)
      }

    }
  }
  return {"name":"Unknown"};
}

function getGroupFromGroupLink(client, groupLinkID){
  for (var i = 0; i < client['groupLinks'].length; i++){
    if (groupLinkID == client['groupLinks'][i]['groupLinkID']){
      return client['groupLinks'][i]['groupID'];
    }
  }
  return undefined;
}

//todo change this to work at the clientID level not through 'name'
function isNameOnline(currentGroupName,clientArray,connectedClientsDetails){
  for (var i = 0; i < clientArray.length; i++){
    if (currentGroupName == clientArray[i]['name']){
      return isClientOnline(clientArray[i]['clientID'],connectedClientsDetails);
    }
  }
  return false;
}

function isClientOnline(currentClientID,connectedClientsDetails){
  for (var i = 0; i < connectedClientsDetails.length; i++){
    //console.log(currentClientID,"!=",connectedClientsDetails);
    if (currentClientID == connectedClientsDetails[i]['clientID']){
      return true;
    }
  }
  return false;
}
//'responseContacts', {"unreadMessageContacts":[],"onlineContacts":contactList,"offlineContacts":[]}

